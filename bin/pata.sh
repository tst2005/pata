#!/bin/sh

STARTPWD="$(pwd)"
cd -- "$(dirname -- "$0")/.." || exit 1
BASEDIR="$(pwd)"
cd -- "$STARTPWD"

PATA_DIR="$BASEDIR"
. "$PATA_DIR/lib/pata.lib.sh"

if [ $# -eq 0 ]; then
	if [ -t 0 ]; then
		echo >&2 "Usage: $0 file"
		exit 1
	fi
	set -- /dev/stdin
fi

A0="$1"
case "$1" in
	(/*) ;;
	(*) f="$STARTPWD/$1"; shift; set -- "$f" "$@" ;;
esac
if [ ! -r "$1" ]; then
	echo >&2 "No such $1"
	exit 1
fi

pata builtin InLoad core/default

pata builtin In 'mods'

# inside $a1 loading, $1 is the next argument
argzero="$1";shift; A0="$A0"
. "$argzero"
